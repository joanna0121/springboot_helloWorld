package com.springboot.springboot_helloworld;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * HelloWorldController测试
 */
public class HellloWorldControllerTest {
	@Test
	public void testSayHello() {
		assertEquals("HelloWorld!", new HelloWorldController().sayHello());
	}

}
